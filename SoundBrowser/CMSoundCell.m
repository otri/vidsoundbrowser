//
//  CMSoundCell.m
//  SoundBrowser
//
//  Created by Aaron Hilton on 11/7/2013.
//  Copyright (c) 2013 Conquer Mobile. All rights reserved.
//

#import "CMSoundCell.h"

@implementation CMSoundCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

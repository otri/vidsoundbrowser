//
//  main.m
//  SoundBrowser
//
//  Created by Aaron Hilton on 11/6/2013.
//  Copyright (c) 2013 Conquer Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CMAppDelegate class]));
    }
}

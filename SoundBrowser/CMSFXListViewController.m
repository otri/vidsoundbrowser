//
//  CMSFXListViewController.m
//  SoundBrowser
//
//  Created by Aaron Hilton on 11/7/2013.
//  Copyright (c) 2013 Conquer Mobile. All rights reserved.
//

#import "CMSFXListViewController.h"
#import "CMSoundCell.h"

@import AVFoundation;

@interface CMSFXListViewController ()
@property(nonatomic, strong) NSArray *soundFilenames;
@property(nonatomic, strong) NSArray *soundDescriptions;
@property(nonatomic, strong) AVAudioPlayer *audioPlayer;

@end

@implementation CMSFXListViewController

- (void) loadTableData {
    NSURL *soundDictURL = [[NSBundle mainBundle] URLForResource:@"SoundFX.plist" withExtension:nil];
    NSDictionary *soundDict = [NSDictionary dictionaryWithContentsOfURL:soundDictURL];
    
    NSMutableArray *filenames = [[NSMutableArray alloc] initWithCapacity:[soundDict count]];
    NSMutableArray *descriptions = [[NSMutableArray alloc] initWithCapacity:[soundDict count]];
    
    NSArray *sortedFilenames = [[soundDict allKeys] sortedArrayWithOptions:NSSortStable usingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
        return [obj1 compare:obj2];
    }];
    
    for( NSString *filename in sortedFilenames ) {
        [filenames addObject:filename];
        [descriptions addObject:soundDict[filename]];
    }
    
    self.soundFilenames = [filenames copy];
    self.soundDescriptions = [descriptions copy];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadTableData];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( section == 0 )
        return [_soundFilenames count];
    else
        return 0;
}

// Plug the area under the translucent status bar with an empty view.
- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 22)];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"soundCell";
    CMSoundCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CMSoundCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:CellIdentifier];
    }
    
    cell.filenameLabel.text = _soundFilenames[indexPath.row];
    cell.descriptionLabel.text = _soundDescriptions[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Play audio file.
    NSString *filename = _soundFilenames[indexPath.row];
    NSURL *resourceURL = [[NSBundle mainBundle] URLForResource:filename withExtension:@".caf"];
    
    NSError *error;
    if( _audioPlayer && [_audioPlayer isPlaying] ) {
        [_audioPlayer stop];
    }
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:resourceURL error:&error];
    [_audioPlayer play];
    
    if( !_audioPlayer ) {
        NSLog(@"CMAVMediaPlayer failed to play audio file \"%@\" with error:\n%@", filename, error);
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

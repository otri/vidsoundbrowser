//
//  CMSoundCell.h
//  SoundBrowser
//
//  Created by Aaron Hilton on 11/7/2013.
//  Copyright (c) 2013 Conquer Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMSoundCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *filenameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@end

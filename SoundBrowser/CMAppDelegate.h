//
//  CMAppDelegate.h
//  SoundBrowser
//
//  Created by Aaron Hilton on 11/6/2013.
//  Copyright (c) 2013 Conquer Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
